﻿namespace hello
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; i < args.Length; i++)
            {
                Console.WriteLine($"Arg {i}: = {args[i]}");
            }

            if (args.Length != 1)
            {
                Console.Write("Wrong number of args. Expecting 1 argument");
                Environment.Exit(-1);
            }

            Console.WriteLine($"Hello {args[0]}");
            Environment.Exit(0);
        }
    }
}
